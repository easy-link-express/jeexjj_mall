/****************************************************
 * Description: ServiceImpl for 内容分类
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.PanelEntity;
import com.xjj.mall.dao.PanelDao;
import com.xjj.mall.service.PanelService;

@Service
public class PanelServiceImpl extends XjjServiceSupport<PanelEntity> implements PanelService {

	@Autowired
	private PanelDao panelDao;

	@Override
	public XjjDAO<PanelEntity> getDao() {
		
		return panelDao;
	}
}