package com.xjj.mall.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import com.xjj.framework.web.support.XJJParameter;
import com.xjj.mall.dao.MemberDao;
import com.xjj.mall.entity.MemberEntity;
import com.xjj.mall.entity.OrderEntity;
import com.xjj.mall.service.RegisterService;


/**
 * @author zhanghejie
 */
@Service
public class RegisterServiceImpl implements RegisterService {
	
	@Autowired
    private MemberDao memberDao;    //用户

	@Override
	public boolean checkData(String param, int type) {

		
		XJJParameter parameter = new XJJParameter();
		parameter.addQuery("query.state@eq@i",1);
		
		
		//1：用户名 2：手机号 3：邮箱
		if (type == 1) {
			//criteria.andUsernameEqualTo(param);
			parameter.addQuery("query.username@eq@s",param);
			
		} else if (type == 2) {
			//criteria.andPhoneEqualTo(param);
			parameter.addQuery("query.phone@eq@s",param);
		} else if (type == 3) {
			//criteria.andEmailEqualTo(param);
			parameter.addQuery("query.email@eq@s",param);
		} else {
			return false;
		}

		List<MemberEntity> memberList = memberDao.findList(parameter.getQueryMap());
		if (memberList != null && memberList.size()>0) {
			return false;
		}
		return true;
	}

	@Override
	public int register(String userName,String userPwd) {

		MemberEntity tbMember=new MemberEntity();
		tbMember.setUsername(userName);

		if(userName.isEmpty()||userPwd.isEmpty()){
			return -1; //用户名密码不能为空
		}
		boolean result = checkData(userName, 1);
		if (!result) {
			return 0; //该用户名已被注册
		}

		//MD5加密
		String md5Pass = DigestUtils.md5DigestAsHex(userPwd.getBytes());
		tbMember.setPassword(md5Pass);
		tbMember.setState(1);
		tbMember.setCreated(new Date());
		tbMember.setUpdated(new Date());

		memberDao.save(tbMember);
		return 1;
	}

	

}
