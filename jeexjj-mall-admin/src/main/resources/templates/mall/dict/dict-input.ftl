<#--
/****************************************************
 * Description: t_mall_dict的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/dict/save" id=tabId>
   <input type="hidden" name="id" value="${dict.id}"/>
   
   <@formgroup title='dict'>
	<input type="text" name="dict" value="${dict.dict}" >
   </@formgroup>
   <@formgroup title='1扩展词 0停用词'>
	<input type="text" name="type" value="${dict.type}" check-type="number">
   </@formgroup>
</@input>